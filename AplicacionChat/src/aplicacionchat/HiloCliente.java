/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacionchat;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;

public class HiloCliente extends Thread {
    
    private Socket SocketCliente;
    private DataInputStream entrada;
    private final Cliente cliente;
    private ObjectInputStream entradaObjeto;
    
    public HiloCliente(Socket socketCliente, Cliente cliente){
        this.SocketCliente = socketCliente;
        this.cliente = cliente;
    }
    
    @Override
    public void run(){
        while(true){
            try{
                entrada = new DataInputStream(SocketCliente.getInputStream());
                cliente.mensajeria(entrada.readUTF());
                
                entradaObjeto = new ObjectInputStream(SocketCliente.getInputStream());
                cliente.actualizarLista((DefaultListModel) entradaObjeto.readObject());
                
            }catch(ClassNotFoundException | IOException ex){
                Logger.getLogger(HiloCliente.class.getName()).log(Level.SEVERE, null,ex);
            }
        }
    }
}
