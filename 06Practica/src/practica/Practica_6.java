
package practica;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.Arrays;
import java.util.Enumeration;
import javax.swing.ButtonGroup;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.WindowConstants;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.io.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Practica_6 {
    JLabel etiqueta1, etiqueta2, etiqueta3,indicador;
    JRadioButton opcion1,opcion2,opcion3;
    JCheckBox opcion4, opcion5, opcion6;
    ButtonGroup grupo1;
    JSlider deslizador;
    JButton btnGenerar;
    JPanel panel;
    FileInputStream entrada;
    FileOutputStream salida;
    public static void main(String[] args) {
        Practica_6 app = new Practica_6();
        app.run();
    }
    public void run(){
        JFrame ventana = new JFrame("PRACTICA 6");
        ventana.setSize(280, 500);
        ventana.setLocationRelativeTo(null);
        ventana.setLayout(null);
        
        grupo1 = new ButtonGroup();
        
        etiqueta1 = new JLabel("Elige un sistema operativo");
        etiqueta1.setBounds(20, 30, 150, 20);
        
        opcion1 = new JRadioButton("Windows");
        opcion1.setBounds(16, 65, 80, 20);
        opcion2 = new JRadioButton("Linux");
        opcion2.setBounds(16, 90, 80, 20);
        
        opcion3 = new JRadioButton("Mac");
        opcion3.setBounds(16, 115, 80, 20);
        
        grupo1.add(opcion1);
        grupo1.add(opcion2);
        grupo1.add(opcion3);
        
        etiqueta2 = new JLabel("Elige tu especialidad");
        etiqueta2.setBounds(20, 160, 150, 20);
        
        opcion4 = new JCheckBox("Programacion");
        opcion4.setBounds(22, 190, 120, 20);
        
        opcion5 = new JCheckBox("Diseño grafico");
        opcion5.setBounds(22, 220, 120, 20);
        
        opcion6 = new JCheckBox("Administracion");
        opcion6.setBounds(22, 250, 120, 20);
        
        etiqueta3 = new JLabel("Horas que dedicas en el ordenador");
        etiqueta3.setBounds(20, 295, 200, 20);
        
        deslizador = new JSlider();
        deslizador.setBounds(40, 340, 200, 20);
        deslizador.setMaximum(24);
        deslizador.setMinimum(0);
        deslizador.setMinorTickSpacing(1);
        deslizador.setValue(12);
        
        indicador = new JLabel(deslizador.getValue()+"");
        indicador.setBounds(20, 340, 20, 20);
        
        btnGenerar = new JButton("Generar");
        btnGenerar.setBounds(90, 400, 80, 20);
        
        
        deslizador.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent ce) {
                int numero = deslizador.getValue();
                indicador.setText(numero+"");
            }
        });
       
        btnGenerar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {

                JRadioButton boton = new JRadioButton();
                boton = botonSeleccionado(opcion1, opcion2, opcion3);
                String casillas = casillasSeleccionadas(opcion4, opcion5, opcion6);
                String contenido = "Tu sistema preferido es "+boton.getText()+", tus especialidades son \n"+casillas+
                        "y el numero de horas dedicadas \nal ordenador son "+indicador.getText();
                JOptionPane.showMessageDialog(null, contenido);
                
                try {
                    guardaArchivo2(contenido);
                    
                } catch (IOException ex) {
                    Logger.getLogger(Practica_6.class.getName()).log(Level.SEVERE, null, ex);
                }

                 
                
            }
            
        });
        
        
        ventana.getContentPane().add(etiqueta1);    
        ventana.getContentPane().add(opcion1);
        ventana.getContentPane().add(opcion2);
        ventana.getContentPane().add(opcion3);
        ventana.getContentPane().add(etiqueta2);
        ventana.getContentPane().add(opcion4);
        ventana.getContentPane().add(opcion5);
        ventana.getContentPane().add(opcion6);
        ventana.getContentPane().add(etiqueta3);
        ventana.getContentPane().add(deslizador);
        ventana.getContentPane().add(indicador);
        ventana.getContentPane().add(btnGenerar);
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); 
    }
    public JRadioButton botonSeleccionado(JRadioButton boton1,JRadioButton boton2,JRadioButton boton3){
        
        if (boton1.isSelected()==true) {
            return boton1;
        }else if (boton2.isSelected()==true) {
            return boton2;
        }else if (boton3.isSelected()==true) {
            return boton3;
        }
        return null;
        
    }
    
    public String casillasSeleccionadas(JCheckBox boton1,JCheckBox boton2,JCheckBox boton3){
        
        
        String oracion=", ";
        if (boton1.isSelected()==true) {
            oracion = boton1.getText()+oracion;
        }
        if (boton2.isSelected() == true) {
            oracion = oracion + boton2.getText();
        }
        if (boton3.isSelected() == true) {
            oracion = oracion + ", "+boton3.getText();
        }
        return oracion;
    }
    
    public void guardarArchivo(File archivo, String contenido){
       
        String respuesta = null;
        
        try {
            salida = new FileOutputStream(archivo);
            byte[] bytesTxt = contenido.getBytes();
            salida.write(bytesTxt);
        } catch (Exception e) {}
    }
    void guardaArchivo2(String mensaje) throws IOException{
        FileWriter archivo;
        try {
            archivo = new FileWriter("respuestas.txt");
            archivo.write(mensaje+"\n");
            archivo.close();
        } catch (IOException ex) {
            Logger.getLogger(Practica_6.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
