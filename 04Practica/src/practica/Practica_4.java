
package practica;

import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Practica_4 {
   JRadioButton radio1;
   JRadioButton radio2;
   JRadioButton radio3;
   JCheckBox casilla1;
   JCheckBox casilla2;
   JCheckBox casilla3;
   JTextField campo1;
   JComboBox caja1;
   JSpinner spin1;
   JLabel original;
   
   JRadioButton radio1E;
   JRadioButton radio2E;
   JRadioButton radio3E;
   JCheckBox casilla1E;
   JCheckBox casilla2E;
   JCheckBox casilla3E;
   JTextField campo1E;
   JComboBox caja1E;
   JSpinner spin1E;
   JLabel espejo;
   
    public static void main(String[] args) {
        Practica_4 app = new Practica_4();
        app.run();
    }
   public void run(){
       
       JFrame ventana = new JFrame("PRACTICA4");
       ventana.setSize(400, 360);
       ventana.setLayout(null);
       ventana.setLocationRelativeTo(null);
       SpinnerNumberModel modeloS = new SpinnerNumberModel();
        
       //----------- ESTA ES LA PARTE ORIGINAL -----------//
       original = new JLabel("ORIGINAL");
       original.setBounds(30, 20, 80, 20);
       
       radio1 = new JRadioButton("Opcion 1");
       radio1.setBounds(50, 60, 100, 20);
       radio2 = new JRadioButton("Opcion 2");
       radio2.setBounds(50, 90, 100, 20);
       
       radio3 = new JRadioButton("Opcion 3");
       radio3.setBounds(50, 120, 100, 20);
       
       casilla1 = new JCheckBox("Opcion 4");
       casilla1.setBounds(160, 60, 100, 20); 
       
       casilla2 = new JCheckBox("Opcion 5");
       casilla2.setBounds(160, 90, 100, 20); 
       
       casilla3 = new JCheckBox("Opcion 6");
       casilla3.setBounds(160, 120, 100, 20);
       
       campo1 = new JTextField();
       campo1.setBounds(260, 60, 80, 20);
       
       caja1 = new JComboBox();
       caja1.setBounds(260, 90, 80, 20);
       
       spin1 = new JSpinner();
       spin1.setBounds(260, 120, 80, 20);
       
       //----------- ESTA ES LA PARTE ESPEJO-----------//
       espejo = new JLabel("ESPEJO");
       espejo.setBounds(30, 160, 80, 20);
       
       radio1E = new JRadioButton("Opcion 1");
       radio1E.setBounds(50, 200, 100, 20);
       radio1E.setEnabled(false);
       
       radio2E = new JRadioButton("Opcion 2");
       radio2E.setBounds(50, 230, 100, 20);
       radio2E.setEnabled(false);
       
       radio3E = new JRadioButton("Opcion 3");
       radio3E.setBounds(50, 260, 100, 20);
       radio3E.setEnabled(false);
       
       casilla1E = new JCheckBox("Opcion 4");
       casilla1E.setBounds(160, 200, 100, 20); 
       casilla1E.setEnabled(false);
       
       casilla2E = new JCheckBox("Opcion 5");
       casilla2E.setBounds(160, 230, 100, 20);
       casilla2E.setEnabled(false);
       
       casilla3E = new JCheckBox("Opcion 6");
       casilla3E.setBounds(160, 260, 100, 20);
       casilla3E.setEnabled(false);
       
       campo1E = new JTextField();
       campo1E.setBounds(260, 200, 80, 20);
       campo1E.setEnabled(false);
       
       caja1E = new JComboBox();
       caja1E.setBounds(260, 230, 80, 20);
       caja1E.setEnabled(false);
       
       spin1E = new JSpinner();
       spin1E.setBounds(260, 260, 80, 20);
       spin1E.setEnabled(false);
       
       radio1.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
               
               if (radio1.isSelected()) {
                   radio1E.setSelected(true);
               }else{
                   radio1E.setSelected(false);
               }
           }
       });
       
       radio2.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
              
               if (radio2.isSelected()) {
                   radio2E.setSelected(true);
               }else{
                   radio2E.setSelected(false);
               }
           }
       });
       radio3.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
              
               if (radio3.isSelected()) {
                   radio3E.setSelected(true);
               }else{
                   radio3E.setSelected(false);
               }
           }
       });
       
       casilla1.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent ae) {
               
               if (casilla1.isSelected()) {
                   casilla1E.setSelected(true);
               }else{
                   casilla1E.setSelected(false);
               }
           }
       });
       
       casilla2.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent ae) {
               
               if (casilla2.isSelected()) {
                   casilla2E.setSelected(true);
               }else{
                   casilla2E.setSelected(false);
               }
           }
       });
       
       casilla3.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent ae) {
               
               if (casilla3.isSelected()) {
                   casilla3E.setSelected(true);
               }else{
                   casilla3E.setSelected(false);
               }
           }
       });
       
       campo1.addKeyListener(new KeyListener() {
           @Override
           public void keyTyped(KeyEvent ke) {
               
           }

           @Override
           public void keyPressed(KeyEvent ke) {
               
               if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                   String dato = campo1.getText();
                   caja1.addItem(dato);
                   caja1.setSelectedItem(dato);
                   caja1E.addItem(dato);
                   caja1E.setSelectedItem(dato);
                   campo1E.setText(dato);
               }
           }

           @Override
           public void keyReleased(KeyEvent ke) {
               
           }
       });
       
       caja1.addActionListener(new ActionListener() {
           @Override
           public void actionPerformed(ActionEvent e) {
              
               Object elemento = caja1.getSelectedItem();
               caja1E.setSelectedItem(elemento);
           }
       });
       
       spin1.addChangeListener(new ChangeListener() {
           @Override
           public void stateChanged(ChangeEvent ce) {
               int numero = (int) spin1.getValue();
               spin1E.setValue(numero);
           }
       });
       
       ventana.getContentPane().add(original);
       ventana.getContentPane().add(radio1);
       ventana.getContentPane().add(radio2);
       ventana.getContentPane().add(radio3);
       ventana.getContentPane().add(casilla1);
       ventana.getContentPane().add(casilla2);
       ventana.getContentPane().add(casilla3);
       ventana.getContentPane().add(campo1);
       ventana.getContentPane().add(caja1);
       ventana.getContentPane().add(spin1);
       ventana.getContentPane().add(espejo);
       ventana.getContentPane().add(radio1E);
       ventana.getContentPane().add(radio2E);
       ventana.getContentPane().add(radio3E);
       ventana.getContentPane().add(casilla1E);
       ventana.getContentPane().add(casilla2E);
       ventana.getContentPane().add(casilla3E);
       ventana.getContentPane().add(campo1E);
       ventana.getContentPane().add(caja1E);
       ventana.getContentPane().add(spin1E);
       
       ventana.setVisible(true);
       ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
       
   }
}
