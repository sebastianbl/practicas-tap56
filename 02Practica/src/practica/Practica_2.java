
package practica;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.WindowConstants;

public class Practica_2 {
    
    JFrame ventana; // Declaramos una ventana tipo JFrame
    JLabel etiqueta1;// Declaramos una etiqueta tipo JLabel llamado etiqueta1
    JLabel etiqueta2;// Declaramos una etiqueta tipo JLabel llamado etiqueta2
    JLabel etiqueta3;// Declaramos una etiqueta tipo JLabel llamado etiqueta3
    JSpinner spin1; // Declaramos un spin1
    JSpinner spin2; // Declaramos un spin2
    JTextField tfCampo;// Declaramos un tfCampo o un campo de texto
    JButton btnGenerar; //Declaramos un boton "generar"
    
    public static void main(String[] args) {
        Practica_2 app = new Practica_2();
        app.run();
    }
    
    void run(){
        
        ventana = new JFrame("Practica 2");//Creamos ventana
        ventana.setLayout(null);// Creamos la plantilla
        ventana.setSize(280, 310);// Definimos el tamaño de la ventana
        ventana.setLocationRelativeTo(null);// La locaclización de la ventana sera en el centro
        
        etiqueta1 = new JLabel("Número1");//Creamos una etiqueta con el texto Numero1
        etiqueta1.setBounds(30, 40, 80, 20);//Definimos posicion (x, y)de la etiqueta con una anchura y altura: (x, y, anchura, altura)
        
        spin1 = new JSpinner();// Creamos el Spinner 1
        spin1.setValue(0);//LO iniciamos con el indice 0.
        spin1.setBounds(150, 40, 80, 20);//Definimos posicion (x, y) del spinner 1 con una anchura y altura: (x, y, anchura, altura)
        etiqueta2 = new JLabel("Número2");//Creamos una etiqueta con el texto Numero2
        etiqueta2.setBounds(30, 100, 80, 20);//Definimos posicion (x, y) de la etiqueta con una anchura y altura: (x, y, anchura, altura)
        
        spin2 = new JSpinner();// Creamos el Spinner 2
        spin2.setValue(0);//LO iniciamos con el indice 0.
        spin2.setBounds(150, 100, 80, 20);//Definimos posicion (x, y) del spinner 2 con una anchura y altura: (x, y, anchura, altura)
        
        etiqueta3 = new JLabel("Número generado");//Creamos una etiqueta con el texto "Número degenerado"
        etiqueta3.setBounds(30, 160, 130, 20);//Definimos posicion (x, y) de la etiqueta con una anchura y altura: (x, y, anchura, altura)
        
        tfCampo = new JTextField();//Creamos el campo de texto
        tfCampo.setBounds(150, 160, 80, 20);//Definimos posicion (x, y) del campo de texto con una anchura y altura: (x, y, anchura, altura)
        tfCampo.setHorizontalAlignment(JTextField.CENTER); //Alineamos el campo de texto en el centro
        
        btnGenerar = new JButton("Generar");// Creamos el botón
        btnGenerar.setBounds(150, 210, 80, 20);//Definimos posicion (x, y) del botón con una anchura y altura: (x, y, anchura, altura)
        
        btnGenerar.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent ae) {
                //Nombre_Clase objeto = new Nombre_Clase();
                Random MiAleatorio = new Random(); //Creamos un objeto "mi aletorio" de tipo random
                int numeroA;
                int numeroB;
                int numeroAleatorio = 0;
                
                numeroA = (int) spin1.getValue();//Obtenemos el valor que esta en campo de texto del spinner 1 y lo convertimos a entero
                numeroB = (int) spin2.getValue();//Obtenemos el valor que esta en campo de texto del spinner 2 y lo convertimos a entero
                
                if (numeroA > numeroB) {
                    numeroAleatorio = (MiAleatorio.nextInt(numeroA-numeroB+1)+numeroB);//El numeroAleatorio tendra un valor con un rango del numeroB hasta el numeroA incluyendo ellos mismo
                }else{
                    if(numeroA < numeroB){
                        numeroAleatorio = (MiAleatorio.nextInt(numeroB-numeroA+1)+numeroA);//El numeroAleatorio tendra un valor con un rango del numeroA hasta el numeroB incluyendo ellos mismo
                    }else{
                        numeroAleatorio = numeroA | numeroB;//El numeroAleatorio tendra el valor del numeroA o NumeroB, ya que son iguales
                    }
                }
                //1° FORMA
                tfCampo.setText(numeroAleatorio+"");//Asignamos el valor de numeroAleatorio al campo de texto tfCampo
                
                //2° FORMA
                //tfCampo.setText(Integer.toString(numeroAleatorio));
            }
            
        });
         
        ventana.getContentPane().add(spin1);//Agregamos el spin1 a la plantilla -> ventana
        ventana.getContentPane().add(etiqueta1);//Agregamos la etiqueta1 a la plantilla -> ventana
        ventana.getContentPane().add(etiqueta2);//Agregamos la etiqueta2 a la plantilla -> ventana
        ventana.getContentPane().add(etiqueta3);//Agregamos la etiqueta3 spin1 a la plantilla -> ventana
        ventana.getContentPane().add(spin2);//Agregamos el spin2 a la plantilla -> ventana
        ventana.getContentPane().add(tfCampo);//Agregamos el campo de texto a la plantilla -> ventana
        ventana.getContentPane().add(btnGenerar);//Agregamos el botón a la plantilla -> ventana
        
        ventana.setVisible(true);//Hacemos visible la ventana
        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);//Cierra la ventana y termina el proceso.
    }
    
}
