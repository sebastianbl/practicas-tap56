package practica;

import java.awt.MenuBar;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class Practica_5 {
    JTextField campo1;
    public static void main(String[] args) {
        Practica_5 p1 = new Practica_5();
        p1.run();
    }
    
    public void run(){
        JFrame ventana = new JFrame("PRACTICA 5");
        ventana.setSize(300, 200);
        ventana.setLocationRelativeTo(null);
        ventana.setLayout(null);
        
        JMenuBar barra = new JMenuBar();
        
        JMenu File = new JMenu("File");
        JMenuItem abrir = new JMenuItem("abrir");
        JMenuItem salir = new JMenuItem("salir");
        
        barra.setBounds(0, 0, 40, 20);
        
        File.add(abrir);
        File.add(salir);
        barra.add(File);
        
        campo1 = new JTextField();
        campo1.setBounds(50, 50, 200, 30);
        
        ventana.getContentPane().add(barra);
        ventana.getContentPane().add(campo1);
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
